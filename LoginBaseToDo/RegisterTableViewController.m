//
//  RegisterTableViewController.m
//  LoginBaseToDo
//
//  Created by APPLE on 28/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import "RegisterTableViewController.h"
#import "Utils.h"
#import "CoreDataManager.h"

@interface RegisterTableViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
- (IBAction)registerClick:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@end

@implementation RegisterTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Validations

- (BOOL)isValidEmail{
    if ([Utils isValidEmail: self.emailTextField.text]) {
        // Check if email exists in core data
        if ([CoreDataManager isEmailExists:self.emailTextField.text]){
            [Utils showAlertWithTitle:@"Error" message:@"Email already exists"];
            return NO;
        }
        
        
    } else {
        [Utils showAlertWithTitle:@"Error" message:@"Enter Valid Email"];
        return NO;
    }
    return YES;
}

- (BOOL)isPasswordValid{
    if ([self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text] && self.passwordTextField.text.length >= 6) {
        return YES;
    } else {
        [Utils showAlertWithTitle:@"Error" message:@"Enter Valid Password. You password and confirm password should match"];
        return NO;
    }
}

- (BOOL)isAllDataFieldsValid{
    BOOL valid = YES;
    valid = [self isValidEmail];
    if (!valid) {
        return NO;
    }
    valid = [self isPasswordValid];
    
    return valid;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}
#pragma UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField isEqual:self.emailTextField] && [self isValidEmail]) {
        [self.userNameTextField becomeFirstResponder];
    }
    return YES;
}





- (IBAction)registerClick:(id)sender {
    if ([self isAllDataFieldsValid]) {
        [CoreDataManager createUserWithEmail:self.emailTextField.text password:self.passwordTextField.text name:self.userNameTextField.text];
        [self.navigationController popViewControllerAnimated:YES];
        [Utils showAlertWithTitle:@"Success" message:@"User created successfully. Please login"];
    } else{
        
    }

    
    
}
@end
