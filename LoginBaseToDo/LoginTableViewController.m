//
//  LoginTableViewController.m
//  LoginBaseToDo
//
//  Created by APPLE on 28/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import "LoginTableViewController.h"
#import "RegisterTableViewController.h"
#import "CoreDataManager.h"
#import "LoggedinUser.h"
#import "Utils.h"
#import "HomeTableViewController.h"

@interface LoginTableViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)registerClick:(id)sender;
- (IBAction)loginClick:(id)sender;
@end

@implementation LoginTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -Validations

-(BOOL)isValid
{
    BOOL valid = YES;
    return valid;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

#pragma mark -IBAction


- (IBAction)registerClick:(id)sender {
    RegisterTableViewController *registerTableViewController =[self.storyboard instantiateViewControllerWithIdentifier:@"register"];
    [self.navigationController pushViewController:registerTableViewController animated:YES];
}

- (IBAction)loginClick:(id)sender {
    User *user =[CoreDataManager loginWithEmail:self.emailTextField.text password:self.passwordTextField.text];
    if (user) {
        LoggedinUser *loggedInUser = [LoggedinUser sharedInstance];
        loggedInUser.user = user;
        HomeTableViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"home"];
        [self.navigationController pushViewController:home animated:YES];
        
        
    } else {
        [Utils showAlertWithTitle:@"Error" message:@"Invalid credentials"];
    }
    
}
#pragma UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField isEqual:self.emailTextField]) {
        [self.passwordTextField becomeFirstResponder];
    } else{
        [textField resignFirstResponder];
    }
    return YES;
}



@end
