//
//  CoreDataManager.h
//  LoginBaseToDo
//
//  Created by APPLE on 28/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
@interface CoreDataManager : NSObject

+ (BOOL)isEmailExists:(NSString *)email;
+ (void)createUserWithEmail:(NSString *)email password:(NSString *)password name:(NSString *)name;
+ (User *)loginWithEmail:(NSString *)email password:(NSString *)password;

@end
