//
//  AddTableViewController.m
//  LoginBaseToDo
//
//  Created by APPLE on 28/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import "AddTableViewController.h"
#import "AppDelegate.h"
#import "ToDo.h"
#import "Utils.h"
#import "LoggedinUser.h"

@interface AddTableViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIActionSheetDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;
@property (weak, nonatomic) IBOutlet UIImageView *addImageView;
@property (strong, nonatomic)UIImagePickerController *imagePickerController;
- (IBAction)pickImage:(id)sender;
@property (strong,nonatomic)NSString *filePath;
@end

@implementation AddTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addSaveButton];
    if (self.toDo!=nil) {
        self.titleTextField.text =self.toDo.title;
        self.descTextView.text =self.toDo.desc;
        NSString *directoryPath = [Utils imageDirectoryPath];
        NSString *imagePath = [directoryPath stringByAppendingPathComponent:self.toDo.imagePath];
        
        UIImage *image = [ UIImage imageWithContentsOfFile:imagePath];
        self.addImageView.image =image;
        self.filePath = self.toDo.imagePath;
    }

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
-(void)addSaveButton
{
    UIBarButtonItem *rightButton =[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save)];
    [self.navigationItem setRightBarButtonItem:rightButton];
}
-(void)save{
    
     AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    if (self.toDo!= nil) {
        self.toDo.title = self.titleTextField.text;
        self.toDo.desc = self.descTextView.text;
        self.toDo.imagePath = self.filePath;
        [appDelegate saveContext];
        [self.delegate addTableViewControllerEdited:self];

    } else{
        NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"ToDo" inManagedObjectContext:appDelegate.managedObjectContext];
        ToDo *toDo = (ToDo *)[[NSManagedObject alloc]initWithEntity:entityDesc insertIntoManagedObjectContext:appDelegate.managedObjectContext];
        
        toDo.title = self.titleTextField.text;
        toDo.desc = self.descTextView.text;
        toDo.imagePath = self.filePath;
        toDo.email = [LoggedinUser sharedInstance].user.email;
        [appDelegate saveContext];
        [self.delegate addTableViewController:self addedToDo:toDo];

    }
    [self.navigationController popViewControllerAnimated:YES];
    
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    
        return 3;
}



- (IBAction)pickImage:(id)sender {
    UIActionSheet *actionSheet= [[UIActionSheet alloc]initWithTitle:@"Image Pick" delegate:self cancelButtonTitle:@"Exit" destructiveButtonTitle:nil otherButtonTitles:@"Photo Lib",@"Cam", nil];
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [self showImagePickerPhotoAlbum];
    } else if (buttonIndex == 1)
    {
        [self showImagePickerCamera];
    }
}

-(void)showImagePickerPhotoAlbum
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate =self;
    imagePickerController.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePickerController animated:YES completion:^{
        
        
    }];
}

-(void)showImagePickerCamera
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imagePickerController animated:YES completion:^{
        
        
    }];
   
}
#pragma mark -UIImagePickerControllerDelegate

 -(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *image= [info objectForKey:UIImagePickerControllerOriginalImage];
    NSString *savePath;
    NSString *directoryPath = [Utils imageDirectoryPath];
    if (self.toDo.imagePath == nil) {
       
        NSArray *fileNamesArray =[Utils listFileAtPath:directoryPath];
        NSInteger maxValue =0;
        for (int i=0; i<fileNamesArray.count; i++) {
            NSString *fileName =[fileNamesArray objectAtIndex:i];
            fileName =[fileName stringByReplacingOccurrencesOfString:@"Image" withString:@""];
            if (maxValue<[fileName integerValue]) {
                maxValue = [fileName integerValue];
            }
        }
        maxValue ++;
        if (![[NSFileManager defaultManager]fileExistsAtPath:directoryPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:NO attributes:nil error:NULL];
        }
        savePath = [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"Image%ld.png",(long)maxValue ]];
        self.filePath = [NSString stringWithFormat:@"Image%ld.png",(long)maxValue ];
    } else {
        savePath = [directoryPath stringByAppendingPathComponent:self.filePath];
    }
    
    [UIImagePNGRepresentation(image) writeToFile:savePath atomically:YES];
    self.addImageView.image = image;
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];

}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
@end
