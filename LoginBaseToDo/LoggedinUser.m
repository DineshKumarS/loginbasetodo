//
//  LoggedinUser.m
//  LoginBaseToDo
//
//  Created by APPLE on 28/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import "LoggedinUser.h"

@implementation LoggedinUser

+ (instancetype)sharedInstance
{
    static LoggedinUser *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LoggedinUser alloc] init];
    });
    return sharedInstance;
}




@end
