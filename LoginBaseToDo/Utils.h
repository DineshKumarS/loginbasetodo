//
//  Utils.h
//  LoginBaseToDo
//
//  Created by APPLE on 28/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Utils : NSObject


+ (BOOL)isValidEmail:(NSString *)checkString;
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;


+ (CGSize)sizeForString:(NSString *)string maxWidth:(CGFloat)maxWidth attributes:(NSDictionary *)attributes;
+ (CGFloat)heightForString:(NSString *)string maxWidth:(CGFloat)maxWidth font:(UIFont *)font;
+(NSArray *)listFileAtPath:(NSString *)path;
+ (NSString *)extractNumberFromText:(NSString *)text;
+(NSString *)imageDirectoryPath;

@end
