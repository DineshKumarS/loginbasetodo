//
//  ToDo+CoreDataProperties.m
//  LoginBaseToDo
//
//  Created by APPLE on 28/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ToDo+CoreDataProperties.h"

@implementation ToDo (CoreDataProperties)

@dynamic title;
@dynamic desc;
@dynamic imagePath;
@dynamic email;

@end
