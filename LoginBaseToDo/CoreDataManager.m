//
//  CoreDataManager.m
//  LoginBaseToDo
//
//  Created by APPLE on 28/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import "CoreDataManager.h"
#import "AppDelegate.h"
@implementation CoreDataManager

+ (BOOL)isEmailExists:(NSString *)email{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"User"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"email == %@",email];
    NSError *error;
    NSArray *resultArray = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (resultArray.count) {
        return YES;
    } else{
        return NO;
    }
}

+ (void)createUserWithEmail:(NSString *)email password:(NSString *)password name:(NSString *)name{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSEntityDescription *userEntity  = [NSEntityDescription entityForName:@"User" inManagedObjectContext:appDelegate.managedObjectContext];
    User *user = (User *)[[NSManagedObject alloc]initWithEntity:userEntity insertIntoManagedObjectContext:appDelegate.managedObjectContext];
    user.email = email;
    user.name = name;
    user.password = password;
    [appDelegate saveContext];
}

+ (User *)loginWithEmail:(NSString *)email password:(NSString *)password{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"User"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"email == %@ AND password == %@",email, password];
    NSError *error;
    NSArray *resultArray = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (resultArray.count) {
        return [resultArray firstObject];
    } else{
        return nil;
    }
    
}

@end
