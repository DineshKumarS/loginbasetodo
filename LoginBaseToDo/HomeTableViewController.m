//
//  HomeTableViewController.m
//  LoginBaseToDo
//
//  Created by APPLE on 28/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import "HomeTableViewController.h"
#import "HomeTableViewCell.h"
#import "AddTableViewController.h"
#import "ToDo.h"
#import "Utils.h"
#import "AppDelegate.h"
#import "User.h"
#import "LoggedinUser.h"

@interface HomeTableViewController ()<UITableViewDataSource,UITableViewDelegate,AddTableViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *homeTableView;

@property(strong,nonatomic)NSArray *dataArray;
@property(strong,nonatomic)NSArray *userArray;
@end

@implementation HomeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addButton];

}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self reloadData];

}

-(void)reloadData
{
    AppDelegate *appDelegate =[UIApplication sharedApplication].delegate;
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"ToDo"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email == %@",[LoggedinUser sharedInstance].user.email];
    [fetchRequest setPredicate:predicate];
    self.dataArray = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [self.homeTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)addButton

{
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc ]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add)];
    [self.navigationItem setRightBarButtonItem:btn ];
}

-(void)add
{
    AddTableViewController *addTVC =[self.storyboard instantiateViewControllerWithIdentifier:@"add"];
    addTVC.delegate =self;
    [self.navigationController pushViewController:addTVC animated:YES];
}

#pragma mark -UITableViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section ==0) {
        return 1;
    }
    else {
        return self.dataArray.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        HomeTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"homelogin" forIndexPath:indexPath];

        LoggedinUser *loggedInUser = [LoggedinUser sharedInstance];
        User *user = loggedInUser.user;
        cell.homeLogin.text = [NSString stringWithFormat:@"logged in as %@",user.name];
        return cell;

    } else{
        HomeTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"home" forIndexPath:indexPath];
        ToDo *todo= [self.dataArray objectAtIndex:indexPath.row];
        cell.titleLabel.text =todo.title;
        cell.descLabel.text = todo.desc;
        NSString *directoryPath = [Utils imageDirectoryPath];
        NSString *imagePath = [directoryPath stringByAppendingPathComponent:todo.imagePath];

        UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
        cell.homeImageView.image =image;
        return cell;
    }
    
   
    
}

#pragma mark -AddTableViewDelegate
-(void)addTableViewController: (AddTableViewController*)addTableViewController addedToDo:(ToDo*)toDo
{
    [self reloadData];
}
-(void)addTableViewControllerEdited:(AddTableViewController *)addTableViewController
{
    [self reloadData];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section ==1)
    {
    ToDo *toDo = [self.dataArray objectAtIndex:indexPath.row];
    [self.homeTableView layoutIfNeeded];
    CGFloat height =57;
    height= height + [Utils heightForString:toDo.title maxWidth:self.homeTableView.frame.size.width-99 font:[UIFont systemFontOfSize:17]];
    height = height + [Utils heightForString:toDo.desc maxWidth:self.homeTableView.frame.size.width-99 font:[UIFont systemFontOfSize:17]];
    return height;
    }
    else if (indexPath.section ==0)
    {
        return 30;
    }
    return 0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    AddTableViewController *addToDoVC =  [self.storyboard instantiateViewControllerWithIdentifier:@"add"];
    addToDoVC.delegate =self;
    ToDo *todo =[self.dataArray objectAtIndex:indexPath.row];
    addToDoVC.toDo =todo;
    [self.navigationController pushViewController:addToDoVC animated:YES];
}


@end
