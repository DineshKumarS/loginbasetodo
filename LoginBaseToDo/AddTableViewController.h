//
//  AddTableViewController.h
//  LoginBaseToDo
//
//  Created by APPLE on 28/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToDo.h"
@class AddTableViewController;
@protocol AddTableViewControllerDelegate<NSObject>

-(void)addTableViewController: (AddTableViewController*)addTableViewController addedToDo:(ToDo*)toDo;
-(void)addTableViewControllerEdited:(AddTableViewController *)addTableViewController;
@end

@interface AddTableViewController : UITableViewController
@property (nonatomic,weak) id<AddTableViewControllerDelegate>delegate;
@property (strong,nonatomic)ToDo *toDo;
@end
