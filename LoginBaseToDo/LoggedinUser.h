//
//  LoggedinUser.h
//  LoginBaseToDo
//
//  Created by APPLE on 28/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface LoggedinUser : NSObject
@property (nonatomic,strong)User *user;

+(instancetype)sharedInstance;

@end
